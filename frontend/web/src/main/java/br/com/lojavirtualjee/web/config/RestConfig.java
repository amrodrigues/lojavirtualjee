package br.com.lojavirtualjee.web.config;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("api/rest")
public class RestConfig extends Application {
	
}

