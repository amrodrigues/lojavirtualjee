package br.com.lojavirtualjee.web.rest;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.lojavirtualjee.backend.entity.Cliente;
import br.com.lojavirtualjee.backend.service.ServiceLojaAPI;

@Path("cliente")
@Produces(MediaType.APPLICATION_JSON)
public class ClienteRest {

	@EJB
	private ServiceLojaAPI<Cliente> serviceLojaAPI;

	@POST
	@Path("/salvar")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response salvarCliente(Cliente cliente) {
		serviceLojaAPI.addEntity(Cliente.class, cliente);
		if (cliente.getIdCliente() == 0) {
		}
		return Response.status(Status.BAD_REQUEST).entity("{\"id\": " + cliente + "}").build();
	}
	
	
}
