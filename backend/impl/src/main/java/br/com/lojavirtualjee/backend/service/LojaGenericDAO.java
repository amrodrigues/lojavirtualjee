package br.com.lojavirtualjee.backend.service;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class LojaGenericDAO<T extends Serializable> implements ServiceLojaAPI<T>{
	
	@PersistenceContext
	private EntityManager entityManager;
		
	@SuppressWarnings({ "unchecked", "hiding" })
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public <T> T addEntity(Class<T> classToCast, Object entity) {
		entityManager.persist(entity);
		return (T) entity;
	}

	@SuppressWarnings("hiding")
	@Override
	public <T> T getEntity(Class<T> classToCast, Serializable pk) {
		 return entityManager.find(classToCast, pk);
	}

	@SuppressWarnings({ "unchecked", "hiding" })
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public <T> T setEntity(Class<T> classToCast, Object entity) {
        Object updatedObj = entityManager.merge(entity);
        return (T) updatedObj;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public void removeEntity(Object entity) {
        Object updateObj = entityManager.merge(entity);
        entityManager.remove(updateObj);
	}

	@SuppressWarnings({ "unchecked", "hiding" })
	@Override
	public <T> List<T> getPureList(Class<T> classToCast, String query, Object... values) {
        Query qr = createQuery(query, values);
        return qr.getResultList();
	}

	@SuppressWarnings({ "unchecked", "hiding" })
	@Override
	public <T> T getPurePojo(Class<T> classToCast, String query, Object... values) {
        Query qr = createQuery(query, values);
        return (T) qr.getSingleResult();
	}

	@Override
	public int executeCommand(String query, Object... values) {
        Query qr = createQuery(query, values);
        return qr.executeUpdate();
	}

	@Override
	public Query createQuery(String query, Object... values) {
        Query qr = entityManager.createQuery(query);
        for (int i = 0; i < values.length; i++) {
            qr.setParameter((i+1), values[i]);
        }
        return qr;
	}

}
