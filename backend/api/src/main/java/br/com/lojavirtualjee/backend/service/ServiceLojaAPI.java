package br.com.lojavirtualjee.backend.service;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;
import javax.persistence.Query;

@Local
public interface ServiceLojaAPI<T> {

	@SuppressWarnings("hiding")
	public <T> T addEntity(Class<T> classToCast,Object entity);
	
	@SuppressWarnings("hiding")
	public <T> T getEntity(Class<T> classToCast,Serializable pk);
	
	@SuppressWarnings("hiding")
	public <T> T setEntity(Class<T> classToCast,Object entity);
	
	public void removeEntity(Object entity);
	
	@SuppressWarnings("hiding")
	public <T> List<T> getPureList(Class<T> classToCast,String query,Object... values);
	
	@SuppressWarnings("hiding")
	public <T> T getPurePojo(Class<T> classToCast,String query,Object... values);
	
	public int executeCommand(String query,Object... values);
	
	public Query createQuery(String query,Object... values);
	
}
