package br.com.lojavirtualjee.backend.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="item_pedido")
@Cacheable
@NamedQueries({
		@NamedQuery(name = "ItemPedido.findAll", query = "SELECT p FROM ItemPedido p", hints = {
				@QueryHint(name = "org.hibernate.cacheable", value = "true") }),
		@NamedQuery(name = "ItemPedido.findById", query = "SELECT p FROM ItemPedido p WHERE p.idItemPedido = :idItemPedido", hints = {
				@QueryHint(name = "org.hibernate.cacheable", value = "true") }) })
public class ItemPedido implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_ITEM_PEDIDO")
	private int idItemPedido;

	private int quantidade;

	//bi-directional many-to-one association to Pedido
	@ManyToOne
	@JoinColumn(name="ID_PEDIDO")
	private Pedido pedido;

	//bi-directional many-to-one association to Produto
	@ManyToOne
	@JoinColumn(name="ID_PRODUTO")
	private Produto produto;

	public ItemPedido() {
	}

	public int getIdItemPedido() {
		return this.idItemPedido;
	}

	public void setIdItemPedido(int idItemPedido) {
		this.idItemPedido = idItemPedido;
	}

	public int getQuantidade() {
		return this.quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public Pedido getPedido() {
		return this.pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public Produto getProduto() {
		return this.produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

}